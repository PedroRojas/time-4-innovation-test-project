import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | abzahlen', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:abzahlen');
    assert.ok(route);
  });
});
