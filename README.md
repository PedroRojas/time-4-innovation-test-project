# test-aufgabe

This README outlines the details of collaborating on this Ember application.
A short introduction of this app could easily go here.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://ember-cli.com/)
* [Google Chrome](https://google.com/chrome/)

## Installation

* `git clone <repository-url>` this repository
* `cd test-aufgabe`
* `npm install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Linting

* `npm run lint:hbs`
* `npm run lint:js`
* `npm run lint:js -- --fix`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

# Documentation
...
## Routes and Components
The app navigation is distributed through 4 [routes](https://i.imgur.com/MhvKYtj.png):

 - ### ItemList - Route: "/"
 **Model:** Loads the saved data (if there is any) from the localStorage using the dataHandler controller.
 **Template:** Renders the **ItemList** component and passes the model data, as well as the FAB button to create new items. 
 - ### AddItem - Route: "/add"
**Model:** *none*
**Template:** Renders the **ItemForm** component
 - ### Sparen - Route: "/sparen"
**Model:** Creates the basic data structure that will eventually be filled by input elements on child components and initializes the values to coincide with the default on the child components:
> let  formData = {
		targetDate:  "",
		isManual:  true,
		interval:  "none"
}

**Template:** Renders the **ItemForm** component and passes the model data to be used by the child components **Toggle Buttons** and **Toggle Interval**
 - ### Abzahlen - Route: "/abzahlen"
**Model:** Creates the basic data structure that will eventually be filled by input elements on child components and initializes the values to coincide with the default on the child components:
> let  formData = {
target:  "",
isManual:  true,
interval:  "none"
}

**Template:** Renders the **ItemForm** component	and passes the model data to be used by the child components **Toggle Buttons** and **Toggle Interval**
