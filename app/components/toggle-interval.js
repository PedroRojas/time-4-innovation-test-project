import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ToggleIntervalComponent extends Component {
    @tracked daily = false;
    @tracked weekly = true;
    @tracked monthly = false;
    @action
    setDaily() {
        let { formData } = this.args;
        this.daily = true;
        this.weekly = false;
        this.monthly = false;
        formData.interval = "daily";
    }
    @action
    setWeekly() {
        let { formData } = this.args;
        this.daily = false;
        this.weekly = true;
        this.monthly = false;
        formData.interval = "weekly";
    }
    @action
    setMonthly() {
        let { formData } = this.args;
        this.daily = false;
        this.weekly = false;
        this.monthly = true;
        formData.interval = "monthly";
    }
}