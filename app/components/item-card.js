import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ItemCardComponent extends Component {
    @tracked collapsed = true;
    @action
    toggleSize() {
        this.collapsed = !this.collapsed;
    }
    @action
    saveSomewhere() {
        console.log("Der Betrag wurde der Zahlungsliste hinzugefügt.");
    }
}