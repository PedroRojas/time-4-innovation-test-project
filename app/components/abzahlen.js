import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import dataHandler from '../controllers/saveDataHandler';

export default class AbzahlenComponent extends Component {
    @action
    saveToStorage() {
        let { formData } = this.args;
        console.log(formData);
        let fee;

        if (formData.isManual) {
            fee = null;
        } else {
            fee = (+formData.target) / 10; //Randomly selected 10 payments.
        }
        let [itemName, itemDate] = localStorage.getItem("nameDate").split(",");
        localStorage.removeItem("nameDate");

        const dataList = [{
            name: itemName,
            startDate: itemDate,
            targetDate: null,
            target: +formData.target,
            paidTotal: 0,
            isManual: formData.isManual,
            feeAmmount: fee,
            interval: formData.interval,
            paymentList: []
        }]

        dataHandler.saveData(dataList);
    }
}