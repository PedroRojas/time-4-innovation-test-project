import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ItemListComponent extends Component {
    @tracked manual = true;

    @action
    setManualPayment() {
        let { abzahlen, formData } = this.args;
        this.manual = true;
        formData.isManual = true;
        formData.interval = "none";
    }

    @action
    setAutomaticPayment() {
        let { abzahlen, formData } = this.args;
        this.manual = false;
        formData.isManual = false;
        formData.interval = "weekly";
    }

}