import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import dataHandler from '../controllers/saveDataHandler';

export default class SparenComponent extends Component {
    // @tracked collapsed = true;
    @action
    saveToStorage() {
        let { formData } = this.args;
        console.log(formData);
        let fee;

        if (formData.isManual) {
            fee = null;
        } else {
            fee = 0
        }
        let [itemName, itemDate] = localStorage.getItem("nameDate").split(",");
        localStorage.removeItem("nameDate");

        const dataList = [{
            name: itemName,
            startDate: itemDate,
            targetDate: formData.targetDate,
            target: 0,
            paidTotal: 0,
            isManual: formData.isManual,
            feeAmmount: fee,
            interval: formData.interval,
            paymentList: []
        }]

        dataHandler.saveData(dataList);
    }
}