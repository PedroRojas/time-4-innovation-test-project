import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ItemFormComponent extends Component {
    @tracked name = "";
    @tracked startDate = "";

    @action
    passParameters() {
        localStorage.setItem('nameDate', this.name + "," + this.startDate);
    }

}