const dummy_data = [{
        name: "Cat",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200,
        paidTotal: 25,
        isManual: true,
        feeAmmount: 0,
        interval: 'none',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 5
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 10
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 10
            },
        ]
    },
    {
        name: "Car",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 20000,
        paidTotal: 30,
        isManual: false,
        feeAmmount: 10,
        interval: 'daily',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 10
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 10
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 10
            },
        ]
    },
    {
        name: "House",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200000,
        paidTotal: 67,
        isManual: true,
        feeAmmount: 0,
        interval: 'none',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 7
            },
        ]
    },
    {
        name: "Plane",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200000,
        paidTotal: 34,
        isManual: true,
        feeAmmount: 0,
        interval: 'none',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 5
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 11
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 18
            },
        ]
    },
    {
        name: "Bike",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200000,
        paidTotal: 90,
        isManual: false,
        feeAmmount: 30,
        interval: 'weekly',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
        ]
    },
    {
        name: "Laptop",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200000,
        paidTotal: 100,
        isManual: true,
        feeAmmount: 0,
        interval: 'none',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 5
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 6
            },
        ]
    },
    {
        name: "Guitar",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200000,
        paidTotal: 11,
        isManual: true,
        feeAmmount: 0,
        interval: 'none',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 5
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 6
            },
        ]
    },
    {
        name: "Kitchen Remodelling",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200000,
        paidTotal: 5,
        isManual: true,
        feeAmmount: 0,
        interval: 'none',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 5
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 6
            },
        ]
    },
    {
        name: "Sofa",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200000,
        paidTotal: 300,
        isManual: false,
        feeAmmount: 100,
        interval: 'monthly',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 100
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 100
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 100
            },
        ]
    },
    {
        name: "Cellphone",
        startDate: (new Date).toLocaleDateString(),
        targetDate: (new Date).toLocaleDateString(),
        target: 200000,
        paidTotal: 41,
        isManual: true,
        feeAmmount: 0,
        interval: 'none',
        paymentList: [{
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 5
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 30
            },
            {
                paymentDate: (new Date).toLocaleDateString(),
                paymentAmmount: 6
            },
        ]
    }
];

export default dummy_data;