import Route from '@ember/routing/route';
import dataHandler from '../controllers/saveDataHandler';

export default class ItemListRoute extends Route {

    model() {
        return dataHandler.loadData();
    }

}