import Route from '@ember/routing/route';

export default class AbzahlenRoute extends Route {

    model() {
        let formData = {
            target: "",
            isManual: true,
            interval: "none"
        }
        return formData
    }

}