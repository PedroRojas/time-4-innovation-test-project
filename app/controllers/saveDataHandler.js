import dummy_data from '../dummy_data/data';

const dataHandler = {
    saveData(newData) {
        let data = JSON.parse(localStorage.getItem('sparData'));
        if (data) { //Check if there is data available in storage
            console.log("Gespeicherte Daten gefunden!");
            data.push(newData[0]); //Combine new and old data
            localStorage.setItem('sparData', JSON.stringify(data)); //Push to storage
        } else {
            console.log("Keine gespeicherten Daten gefunden.");
            localStorage.setItem('sparData', JSON.stringify(newData)); //Push to storage
        }
    },
    loadData() {
        let data = JSON.parse(localStorage.getItem('sparData'));
        if (data) {
            console.log("Gespeicherte Daten gefunden!");
            return data;
        } else {
            console.log("Keine gespeicherten Daten gefunden. Verwendung von Dummy-Daten:");
            return dummy_data;
        }
    }
}

export default dataHandler;