import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
    location = config.locationType;
    rootURL = config.rootURL;
}

Router.map(function() {
    this.route('itemList', { path: '/' });
    this.route('addItem', { path: '/add' });
    this.route('sparen');
    this.route('abzahlen');
    this.route("itemList", { path: '/*' });
});